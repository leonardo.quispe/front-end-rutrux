import axios from 'axios';

export class ConsultaService {
   
    baseUrl = "http://localhost:8080/api/v1/";

    getAll(){
        return axios.get(this.baseUrl + "all").then(res => res.data);
    }
    
    save(consulta){
        return axios.post(this.baseUrl + "save", consulta).then(res => res.data);
    }
}