import React from 'react';
import Menu from '../menu/Menu';
import Formulario from './formulario/Formulario';
import Mapa from './mapa/Mapa';
import Consultas from './consultas/Consultas';
import Footer from '../footer/Footer';

class Contacto extends React.Component {
	render() {
		return(
			<>
			<Menu />
			<main role="main" class="flex-shrink-0 mt-5">
				<div class="container px-5">
					<h1 class="px-5 mb-5"><b>Consúltanos</b></h1>
            		<div class="row">
            			<div class="px-5 col-md-6">
							<Formulario />
						</div>
						<div class="px-5 col-md-6">
							<Mapa />
						</div>
						<hr className="featurette-divider" />
					</div>
					<div class="container px-5">
					<h1 class="px-5 text-center py-5"><b>Ultimas Consultas</b></h1>
						<Consultas />
					</div>
					<hr className="featurette-divider" />
				</div>	
	  		</main>
	  		<Footer />
	  		</>
		)
	}
}
export default Contacto;