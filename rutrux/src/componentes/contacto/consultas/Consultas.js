import React from 'react';
import '../../../App.css';
import { ConsultaService } from '../../../service/ConsultaService';

class Consultas extends React.Component {
    constructor(props){
        super(props);
        this.state = ({
            consultas: [],
            pos: null,
            id: 0,
            completo: '',
            correo: '',
            asunto: '',
            mensaje : ''
          
        });
        this.consultaService = new ConsultaService();
    }

    componentDidMount(){
        this.consultaService.getAll().then(data => this.setState({consultas: data}))
    }

    render() {
        return (
            <div class="container px-5">
                <div class="container px-5">
                    {this.state.consultas.map((consulta) =>{
                    return (
                        <div class="card text-center">
                            <div class="card-header">
                                <h4><b>{consulta.completo}</b></h4>
                            </div>
                            <div class="card-body">
                                <h5 class="card-title">{consulta.asunto}</h5>
                                <p class="card-text">{consulta.mensaje}</p>
                            </div>
                            <div class="card-footer text-muted">
                                <h6>Email: {consulta.correo}</h6>
                            </div>
                        </div>
                        
                    );
                    })}
                </div>
            </div>
        )
    }

}
export default Consultas;