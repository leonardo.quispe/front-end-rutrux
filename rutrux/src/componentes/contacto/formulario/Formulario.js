import React from 'react';
import '../../../App.css';
import { ConsultaService } from '../../../service/ConsultaService';

class Formulario extends React.Component {
  constructor(){
    super();
    this.state = {
      consulta: {
        id: null,
        completo: null,
        correo: null,
        asunto: null,
        mensaje : null
      },
      selectedConsulta : {

      }
    };
    this.consultaService = new ConsultaService();
    this.save = this.save.bind(this);
  }

  componentDidMount(){
    this.consultaService.getAll().then(data => this.setState({consultas: data}))
  }
 
  save(){
    this.consultaService.save(this.state.consulta).then(data =>{
      this.setState({
        consulta: {
          id: null,
          completo: null,
          correo: null,
          asunto: null,
          mensaje : null
        }
    });
      this.showSaveDialog();
      this.borrar();
      this.consultaService.getAll().then(data => this.setState({consultas: data}));
    })
  }


  render() {
    return (
        <form class="mb-5" id="consulta-form">
          <div class="form-group">
            <label  class="h6"><b>Nombres y Apellidos</b></label>
            <input type="text" value={this.state.consulta.completo} class="form-control" id="completo" onChange={(e) => {
              let val = e.target.value;
              this.setState(prevState => {
                let consulta = Object.assign({}, prevState.consulta);
                consulta.completo = val;
                
                return {consulta};
            })}}
            />            
          </div><br/>

          <div class="form-group">
            <label class="h6"><b>Email</b></label>
            <input type="text"  value={this.state.consulta.correo} class="form-control" id="correo" onChange={(e) => {
              let val = e.target.value;
              this.setState(prevState => {
                let consulta = Object.assign({}, prevState.consulta);
                consulta.correo = val;
                
                return {consulta};
            })}}
            />
          </div><br/>

          <div class="form-group">
            <label class="h6"><b>Asunto</b></label>
            <input type="text" value={this.state.consulta.asunto} class="form-control" id="asunto" onChange={(e) => {
              let val = e.target.value;
              this.setState(prevState => {
                let consulta = Object.assign({}, prevState.consulta);
                consulta.asunto = val;
                
                return {consulta};
            })}}
            />
          </div><br/>

          <div class="form-group">
            <label class="h6"><b>Mensaje</b></label>
            <textarea ref="mens" value={this.state.consulta.mensaje} class="form-control" id="mensaje" onChange={(e) => {
              let val = e.target.value;
              this.setState(prevState => {
                let consulta = Object.assign({}, prevState.consulta);
                consulta.mensaje = val;
                
                return {consulta};
            })}}
            />
          </div><br/>

          <button type="submit" class="btn btn-primary" onClick={this.save} ><b>Enviar</b></button>
        </form>
    )
  }
  showSaveDialog(){
    document.getElementById("consulta-form").reset();
  }
  borrar(){
    this.refs.mens.value="";;
  }
}
export default Formulario;