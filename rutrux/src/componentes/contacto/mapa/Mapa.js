import React from 'react';

class Mapa extends React.Component {
  render() {
    return (
        <div class="container embed-responsive embed-responsive-4by3 px-5">
            <iframe width="360" height="400" class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d6391.3803501791535!2d-79.0443199603627!3d-8.148638672406342!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x91ad3d47e1fef7f9%3A0xc69536bb4839b47b!2sInstituto%20Superior%20Tecsup!5e0!3m2!1ses!2spe!4v1621045936548!5m2!1ses!2spe"></iframe>
        </div>
    )
  }
}
export default Mapa;