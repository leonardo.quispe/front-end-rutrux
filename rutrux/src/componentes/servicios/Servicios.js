import React from 'react';
import Menu from '../menu/Menu';
import Footer from '../footer/Footer';
import './../../App.css'
import Carrusel from './carrusel/Carrusel';
 
class Servicios extends React.Component {
	render() {
		return(
			<>
			<Menu />
                <div class="rutrux py-5">
                    <Carrusel />
                </div>
                
		        <div class="container px-5 py-5">
                    <section class="container px-5">
                        <div class="container text-center">
                            <div class="col-12">
                                <h1 class=""><b>RuTrux tiene</b></h1>
                                <p class="h5" style={{ color: "#5D5F5E" }}>Acá una breve lista de lo que ofrecemos</p>
                            </div>
                        </div><br/>
                        
                        <div class="container py-4">
                            <ul class="container list-group list-group-flush">
                                <li class="list-group-item">
                                    <div class="container row">
                                        <div class="col-md-11 px-5">
                                            <p class="h6">Rutas completas de microbuses en Trujillo</p>
                                        </div>
                                        <div class="col-md-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-lg" viewBox="0 0 16 16">
                                                <path d="M13.485 1.431a1.473 1.473 0 0 1 2.104 2.062l-7.84 9.801a1.473 1.473 0 0 1-2.12.04L.431 8.138a1.473 1.473 0 0 1 2.084-2.083l4.111 4.112 6.82-8.69a.486.486 0 0 1 .04-.045z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </li><br/>
                               
                                <li class="list-group-item">
                                    <div class="container row">
                                        <div class="col-md-11 px-5">
                                            <p class="h6">80% de la información del transporte público de Trujillo</p>
                                        </div>
                                        <div class="col-md-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-lg" viewBox="0 0 16 16">
                                                <path d="M13.485 1.431a1.473 1.473 0 0 1 2.104 2.062l-7.84 9.801a1.473 1.473 0 0 1-2.12.04L.431 8.138a1.473 1.473 0 0 1 2.084-2.083l4.111 4.112 6.82-8.69a.486.486 0 0 1 .04-.045z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </li><br/>

                                <li class="list-group-item">
                                    <div class="container row">
                                        <div class="col-md-11 px-5">
                                            <p class="h6">Información Veridica y Actualizada</p>
                                        </div>
                                        <div class="col-md-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-lg" viewBox="0 0 16 16">
                                                <path d="M13.485 1.431a1.473 1.473 0 0 1 2.104 2.062l-7.84 9.801a1.473 1.473 0 0 1-2.12.04L.431 8.138a1.473 1.473 0 0 1 2.084-2.083l4.111 4.112 6.82-8.69a.486.486 0 0 1 .04-.045z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </li><br/>

                                <li class="list-group-item">
                                    <div class="container row">
                                        <div class="col-md-11 px-5">
                                            <p class="h6">Compatibilidad con el 84% de Smartphone</p>
                                        </div>
                                        <div class="col-md-1">
                                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-check-lg" viewBox="0 0 16 16">
                                                <path d="M13.485 1.431a1.473 1.473 0 0 1 2.104 2.062l-7.84 9.801a1.473 1.473 0 0 1-2.12.04L.431 8.138a1.473 1.473 0 0 1 2.084-2.083l4.111 4.112 6.82-8.69a.486.486 0 0 1 .04-.045z"/>
                                            </svg>
                                        </div>
                                    </div>
                                </li>				
                            </ul>
                        </div>
                    </section>  
			    </div>  
                <hr className="featurette-divider" /> 
	  		<Footer />
	  		</>
		)
	}
}
export default Servicios;