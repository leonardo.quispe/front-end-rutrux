import React from 'react';
import img9 from './../../../assets/img/2.jpeg';
 
class Carrusel extends React.Component {
  render() {
  	return (
        <div class="py-5">
            <div class="container text-center">
                <div class="">
                    <h1 class="" style={{color:'#77f7e8'}}><b>¿Cómo funciona?</b></h1>
                    <p class="h5" style={{color:'#FFFFFF'}}>Aquí veras la funcionabilidad de RuTrux</p>
                </div>
            </div><br/>
            <div id="funciones" class="container px-5 py-4 carousel slide" data-bs-ride="carousel">
                <div class="container carousel-inner">
                    <div class="carousel-item active">
                        <img src={img9} height="450" class="mx-auto d-block mw-90" alt="..."/>
                        <div class="carousel-caption d-none d-md-block">
                        <h4 style={{color:'#77f7e8'}}><b>First slide label</b></h4>
                        <p class="h6"><b>Some representative placeholder content for the first slide.</b></p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src={img9}  height="450" class="mx-auto d-block mw-90" alt="..."/>
                        <div class="carousel-caption d-none d-md-block">
                        <h4 style={{color:'#77f7e8'}}><b>Second slide label</b></h4>
                        <p class="h6"><b>Some representative placeholder content for the second slide.</b></p>
                        </div>
                    </div>
                    <div class="carousel-item">
                        <img src={img9} height="450"  class="mx-auto d-block mw-90" alt="..."/>
                        <div class="carousel-caption d-none d-md-block">
                        <h4 style={{color:'#77f7e8'}}><b>Third slide label</b></h4>
                        <p class="h6"><b>Some representative placeholder content for the third slide.</b></p>
                        </div>
                    </div>
                    <div class="carousel-indicators">
                        <button type="button" data-bs-target="#funciones" data-bs-slide-to="0" class="active" aria-current="true" aria-label="Slide 1"></button>
                        <button type="button" data-bs-target="#funciones" data-bs-slide-to="1" aria-label="Slide 2"></button>
                        <button type="button" data-bs-target="#funciones" data-bs-slide-to="2" aria-label="Slide 3"></button>
                    </div>
                </div>
                <button class="carousel-control-prev" type="button" data-bs-target="#funciones" data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                </button>
                <button class="carousel-control-next" type="button" data-bs-target="#funciones" data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                </button>
            </div>
            
        </div>
  	)
  }
}
export default Carrusel;