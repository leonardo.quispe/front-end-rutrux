import React from 'react';
import { NavLink } from 'react-router-dom';
import logo from './../../assets/img/logo.png';
import './../../App.css'
 
class Menu extends React.Component {
  render() {
  	return (
      <header class="rutrux" >
        <div class="container-fluid px-5 py-4">
          <nav class="navbar navbar-dark navbar-expand-lg px-5" >
              <div class="container-fluid px-5 mx-5">
                      <NavLink to="/" class="navbar-brand"><img src={logo} width="285" height="70"/></NavLink>
                <div class="navbar-nav">
                      <NavLink to="/" class="nav-link lead" style={{ color: "#FFFFFF" }}><b class="h5">Inicio</b></NavLink>
                      <NavLink to="/nosotros" class="nav-link lead" style={{ color: "#FFFFFF" }}><b class="h5">Nosotros</b></NavLink>                        
                      <NavLink to="/servicios" class="nav-link lead" style={{ color: "#FFFFFF" }}><b class="h5">Servicios</b></NavLink>    
                      <NavLink to="/contacto" class="nav-link lead" style={{ color: "#FFFFFF" }}><b class="h5">Consúltanos</b></NavLink> 
                </div>		
              </div>
          </nav>
        </div>
      </header>
  	)
  }
}
export default Menu;
