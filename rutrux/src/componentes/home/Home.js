import React from 'react';
import Menu from '../menu/Menu';
import Silder from './silder/Silder';
import Footer from '../footer/Footer'; 
import Preguntas from './preguntas/Preguntas';
 
class Home extends React.Component {
	render() {
		return(
			<>
			<Menu /> 
			<main role="main">
		        <div class="">
                <Silder />
				</div>
				<div class="container px-5 pm-5">
				<Preguntas />
                <hr className="featurette-divider" />
		        </div>
	  		</main>
	  		<Footer />
	  		</>
		)
	}
}
export default Home;