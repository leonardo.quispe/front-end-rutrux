import React from 'react';
import img7 from './../../../assets/img/7.png';
import img9 from './../../../assets/img/9.png';
import img10 from './../../../assets/img/10.png';
import img11 from './../../../assets/img/11.png';
import img12 from './../../../assets/img/12.png';
import './../../../App.css'

class Silder extends React.Component {
  render() {
  	return (
        <div>
            <section class="rutrux" >
                    <div class="container">
                        <div class="row px-5 mx-4 py-4">
                            <div class="col-md-6 px-5 py-5 my-5">
                                <h1 class="" style={{color:'#77f7e8'}}><b>TODAS LAS RUTAS<br/>EN UNA SOLA APP</b></h1><br/>
                                <h5 class="" style={{color:'#FFFFFF'}}>Muévete por todo Trujillo en microbus<br/>sin nigún problema</h5>
                                <div class="d-grid gap-2 col-6 mx-5 px-3"><br/>
                                    <a href="https://play.google.com/store/apps/details?id=pe.tumicro.android" target="_blank">
                                    <input type="button" class="btn btn-light btn-lg" value="Descarga Play Store"></input></a>
                                </div>
                            </div>
                            
                            <div class="col-md-6">
                                <div class="px-5 py-5">
                                    <img src={img7}  width="340" height="420" alt="Screenshot of Android App" class=""/>
                                </div>
                            </div>
                        </div>
                    </div>
            </section>
            
            <section class="rutrux">
                <div class="container py-4">
                    <div class="row px-5 mx-4 py-5">
                        <div class="col-md-6 px-5 py-5">
                            <div class="row">
                                <div class="col-md-2 my-3">
                                    <img class="rounded float-start" src={img10} width="46" height="50"/>
                                </div>
                                <div class="col-md-10">
                                    <h4 class="" style={{color:'#77f7e8'}}><b>Todas la Rutas</b></h4>
                                    <p class="h6" style={{color:'#FFFFFF'}}>Busca la ruta completa del microbus que desees, 
                                                con tan solo seleccionar la Empresa y linea correspondientes.</p>
                                </div>
                            </div><br/>
                            
                            <div class="row">
                                <div class="col-md-2 my-3">
                                    <img class="rounded float-start" src={img11} width="46" height="46"/>
                                </div >
                                <div class="col-md-10">
                                    <h4 class="" style={{color:'#77f7e8'}}><b>Información Actualizada</b></h4>
                                    <p class="h6" style={{color:'#FFFFFF'}}>En el apartado de detalles, podras obtener información actualizada, como por ejemplo: 
                                                El paradero, el costo de pasajes, entre otros.</p>
                                </div>
                            </div><br/>
                            
                            <div class="row">
                                <div class="col-md-2 my-3">
                                    <img class="rounded float-start" src={img12} width="55" height="49"/>
                                </div>
                                <div class="col-md-10">
                                    <h4 class="" style={{color:'#77f7e8'}}><b>Facilidad de Uso</b></h4>
                                    <p class="h6" style={{color:'#FFFFFF'}}>RuTrux es una app muy facil de usar, ademas cuenta con un apartado en el que podras ver 
                                                las ultimas rutas buscadas y asi evitar volver a buscarlas.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="px-4">
                                <img src={img9}  width="380" height="480" alt="Screenshot of Android App" class=""/>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
  	)
  }
}
export default Silder;