import React from 'react';

class Preguntas extends React.Component {
  render() {
    return (
        <div class="container py-3 pm-5 px-5">
            <section class="container py-5 pm-5 px-5">
			    <div class="container">
				    <div class="col-12 text-center">
					    <h1 class="" ><b>Preguntas Frecuentes</b></h1><br/>
					    <p class="h5" style={{ color: "#5D5F5E" }}>Si no encuentras una respuesta a tu pregunta,
									no dudes en contactarnos</p>
				    </div>
			    </div><br/>
			
                <div class="container pm-5 px-5">
                    <div class="container px-5">
                        <div class="">
                            <h5><b class=""> ¿RuTrux es gratis? </b></h5>
                            <p class=" mx-3 h6">Sí.</p>
                        </div><br/>
                        
                        <div class="">
                            <h5><b class=""> ¿En qué plataformas está disponible RuTrux? </b></h5>
                            <p class="mx-3 h6">Por el momento solo está disponible en Android.</p>
                        </div><br/>
                        
                        <div class="">
                            <h5><b class=""> ¿Dónde puedo usar RuTrux? </b></h5>
                            <p class="mx-3 h6">RuTrux funciona principalmente en la ciudad de Trujillo, sin embargo, si tú quieres
                                        que la información de tu ciudad este en RuTrux no dudes en escribirnos a hola@RuTrux.pe.</p>
                        </div><br/>
                        
                        <div class="">
                            <h5><b class=""> ¿En qué se diferencia RuTrux de otras aplicaciones? </b></h5>
                            <p class="mx-3 h6">TuRuta es la única aplicación con el 80% de la información del transporte público en
                                        Trujillo, el resto te dirá rutas que no necesariamente son las actuales. 
                                        Además, RuTrux te brinda información 100% veridica y actualizada.</p>
                        </div><br/>
                        
                        <div class="">
                            <h5><b class=""> ¿Para quiénes está pensado RuTrux? </b></h5>
                            <p class="mx-3 h6">Si te mueves por la ciudad, RuTrux es para tí.</p>
                        </div><br/>
                        
                        <div class="">
                            <h5><b class=""> ¿Qué se necesita para usar RuTrux? </b></h5>
                            <p class="mx-3 h6">Un smartphone, conexión a internet y ganas de pasear por la ciudad.</p>
                        </div><br/>
                    </div>
			    </div>
		    </section>
        </div>
    )
  }
}
export default Preguntas;