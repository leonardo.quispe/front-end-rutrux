import React from 'react';
import './../../App.css'
 
class Footer extends React.Component {
  render() {
    return (
		<footer class="rutrux" style={{display: 'flex',  justifyContent:'center', alignItems:'center', height: '15vh'}}>
			<div class="">
				<div class="container">
					<span class="span" style={{ color: "#FFFFFF" }}>©</span> 
					<span class="span" style={{ color: "#FFFFFF" }}>RuTrux S.A.C 2021</span>
				</div>
			</div>
		</footer>
    )
  }
}
export default Footer;