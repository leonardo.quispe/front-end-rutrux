import React from 'react';
import Menu from '../menu/Menu';
import Familia from './familia/Familia';
import Donar from './donar/Donar';
import Footer from '../footer/Footer';
import './../../App.css'

class Nosotros extends React.Component {
	render() {
		return(
			<>
			<Menu />
			<main role="main" class="">
                <div class="rutrux">
				    <Familia />
                </div>
                <div class="py-5">
				    <Donar />
                    <hr className="featurette-divider" />
                </div>	
	  		</main>
	  		<Footer />
	  		</>
		)
	}
}
export default Nosotros;