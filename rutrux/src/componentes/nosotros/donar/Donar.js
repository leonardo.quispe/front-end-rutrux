import React from 'react';
import img3 from './../../../assets/img/3.png';
import img4 from './../../../assets/img/4.png';
import img5 from './../../../assets/img/5.png';
import img6 from './../../../assets/img/6.png';
import img13 from './../../../assets/img/13.png';
import img14 from './../../../assets/img/14.jpg';
import img15 from './../../../assets/img/15.png';
import img16 from './../../../assets/img/16.png';

class Donar extends React.Component {
  render() {
    return (
        <div class="py-5">
            <section class="">
                <div class="container text-center mb50">
                    <div class="col-12">
                        <h1 class=""><b>Con el apoyo de</b></h1><br/>
                        <p class="h5" style={{ color: "#5D5F5E" }} >RuTrux cuenta con el apoyo de todas las empresas de transporte de Trujillo<br/>aqui algunas de ellas:</p>
                    </div>
                </div><br/>
                
                <div class="container px-5">
                    <div class="row px-5">
                        <div class="col-3">
                            <img src={img3} width="180" height="65" alt="Empresa 1" />
                        </div>
                        <div class="col-3">
                            <img src={img4} width="180" height="65" alt="Empresa 2" />
                        </div>
                        <div class="col-3">
                            <img src={img6} width="180" height="70" alt="Empresa 3" />
                        </div>
                        <div class="col-3">
                            <img src={img5} width="180" height="70" alt="Empresa 4" />
                        </div>
                    </div>
                </div>
            </section>

            <section class="py-5">
                <div class="py-5"> 
                    <div class="container text-center mb50 py-4">
                        <div class="">
                            <div class="">
                                <img src={img13} width="80" height="80" alt="Screenshot of application"/>
                            </div>
                        </div>
                    </div>
                    <div class="container text-center mb50">
                        <div class="col-12 ">
                            <h1 class=""><b>¡Dona y ayúdanos a seguir<br/> mejorando!</b></h1><br/>
                            <p class="h5" style={{ color: "#5D5F5E" }} >Queremos que tu experiencia en RuTrux sea la mejor, por eso ahora puedes<br/>  apoyarnos
                                        a seguir avanzando. Puedes donar lo que quieras y así empujarnos<br/> a llegar más lejos.
                                        Todos los aportes son valiosos y nos ayudarán a mejorar.</p>
                        </div>
                    </div>
                </div>
		    </section><br/>
		
            <section class="container px-5">
                <div class="row container flex px-5">
                    <div class="col-8 py-5">
                        <div class="flex mobile-flex-wrap mb20 mobile-text-center px-5">
                            <div>
                                <h4 class=""><b>Número de Cuenta BCP Soles</b></h4>
                                <p class="h5" style={{ color: "#5D5F5E" }}>Número de cta: 143-2785705-0-32<br/>
                                            Número de cta. interbancaria:002-15248536957812457</p>
                            </div>
                        </div>
                        
                        <div class="flex mobile-flex-wrap mb20 mobile-text-center px-5">
                            <div>
                                <h4 class=""><b>Número de Cuenta BCP Dólares</b></h4>
                                <p class="h5" style={{ color: "#5D5F5E" }}>Número de cta: 143-2785705-0-32<br/>
                                            Número de cta. interbancaria:002-15248536957812457</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="mx-5">
                            <img src={img14} width="230" height="260" alt="Screenshot of Android App" />
                        </div>
                    </div>
                </div>
            </section><br/>
		
            <section class="py-4">
                <div class="container text-center mb50">
                    <div class="">
                        <h1 class=""><b>Otros métodos de pago</b></h1>
                    </div>
                </div><br/>
                
                <div class="container py-4 px-5">
                    <div class="row container flex center-horizontal wrap center-vertical px-5">
                        <div class="col-3"></div>
                        <div class="col-3 px-4">
                            <a href="https://play.google.com/store/apps/details?id=com.bcp.innovacxion.yapeapp" target="_blank">
                                <img src={img15} width="200" height="100" alt="PayPal"/>						
                            </a>
                        </div>
                        
                        <div class="col-3 mx-4">
                            <a href="http://smarturl.it/DonaPayPal" target="_blank">
                                <img src={img16} width="180" height="120" alt="yapeapp"/>	
                            </a>
                        </div>
                        <div class="col-3"></div>
                    </div>
                </div>
            </section>
		
            <section class="py-5">
                <div class="container text-center mb50 py-4">
                    <div class="col-12">
                        <h1 class=""><b>¡Sé parte del cambio!</b></h1><br/>
                        <p class="h5" style={{ color: "#5D5F5E" }}>Si donaste o si te gustaría apoyarnos de alguna manera puedes<br/> dejar tus datos aquí o
                                    escribirnos a hola@RuTrux.pe. Seguiremos<br/> trabajando para alcanzar nuestro objetivo de
                                    una mejor ciudad,<br/> para que puedas movilizarte a donde quieras de forma rápida y <br/>segura.</p>
                    </div>
                </div>
                
                <div class="container">
                    <div class="row container text-center">
                        <div class="col-12">
                            <a href="/#/contacto">
                            <input type="button" class="btn btn-secondary" value="Dejanos tus datos aqui!"></input></a>
                        </div>
                    </div>
                </div>
            </section>
    </div>
    ) 
  }
}
export default Donar;