import React from 'react';
import img1 from './../../../assets/img/1.png';
import img2 from './../../../assets/img/2.jpeg';
import './../../../App.css'

class Familia extends React.Component {
  render() {
    return (
        <div class="container py-5">
            <section class="">
                <div class="container text-center mb50 py-4">
                    <div class="">
                        <div class="">
                            <img src={img1} width="63" height="61" alt="Screenshot of application"/>
                        </div>
                    </div>
                </div>

                <div class="container py-4">
                    <div class="container text-center mb50">
                        <h1 class="" style={{color:'#77f7e8'}}><b>Familia RuTrux</b></h1><br/>
                        <p class="h4" style={{color:'#FFFFFF'}}>RuTrux es antes que nada un grupo de personas apasionadas por<br/> cambiar la realidad en la
                                                                    que vivimos para generar eficiencia y<br/> calidad de vida.</p>
                    </div>
                </div><br/>

                <div class="container text-center mb50">
                    <div class="">
                        <div class="mx-2">
                            <img src={img2}  width="1000" height="500" alt="Screenshot of application" />
                        </div>
                    </div>
                </div>
                <div class="py-4"></div>
            </section>
        </div>
    )
  }
}
export default Familia;